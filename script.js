//Counter to continously play the audio cliü
soundCounter = 0;

// Set the date we're counting down to
var countDownDate = new Date("Jul 15, 2018 01:45:00").getTime();

/* Make side-note visible / invissible */
function visi() {
    document.getElementById('side-note').style.visibility = 'visible';
}

function invisi() {
    document.getElementById('side-note').style.visibility = 'hidden';
}

/* Event Listener um die Side-Note zu checken */
document.getElementById('cd').addEventListener('mouseenter', visi())

document.getElementById('cd').addEventListener('mouseleave', invisi())

// Update the count down every 1 second
var ticker = setInterval(() => {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    //Format the Days
    var days = ("0" + days).slice(-2);
    //Format the Hours
    var hours = ("0" + hours).slice(-2);
    //Format the Minutes
    var minutes = ("0" + minutes).slice(-2);
    //Format the Seconds
    var seconds = ("0" + seconds).slice(-2);

    // Output the result in an element with id="demo"
    document.getElementById("cd").innerHTML = days + " : " + hours + " : "
    + minutes + " : " + seconds;

    // starting sound, overlapping each other because then it's smooth
    if(soundCounter % 4 <= 1){
        console.log("Play 1");
        document.getElementById("tick1").play();
    }
    else if(soundCounter % 4 >= 2){
        console.log("Play 2");
        document.getElementById("tick2").play();
    }
    soundCounter++; 

    // If the count down is over, write some text 
    if (distance < 0) {
        ende();
    }
}, 1000);

function ende(){
    /* Uhr anhalten */
    clearInterval(ticker);

    /* Musik Stoppen */
    document.getElementById("tick1").pause();
    document.getElementById("tick2").pause();

    /* Uhr ersetzen mit fancy animiertem text*/
    document.getElementById("cd").innerHTML = "PUGIO SHOULD HAVE ARRIVED BY NOW!";
    document.getElementById("cd").id = "animate";

    /* Side Note Erscheinen lassen */
    visi();
    document.getElementById('cd').removeEventListener('mouseenter', visi());
    document.getElementById('cd').removeEventListener('mouseleave', invisi());
}


/* Developer Button */
/*
document.getElementById("develop").addEventListener("click", () => {
    ende();
})
*/